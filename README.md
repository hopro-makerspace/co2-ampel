# co2 ampel



## Overview
The project has been realized with the [Tinkerforge](https://www.tinkerforge.com) building block. We measure the CO2 concentration with a [CO2-Bricklet](https://www.tinkerforge.com/en/doc/Hardware/Bricklets/CO2_V2.html). The [RGB-LED](https://www.tinkerforge.com/en/doc/Hardware/Bricklets/RGB_LED_V2.html) indicates the values with three colors:

- green, if the concentration is below 900 pm
- orange for a concentration between 900 pmm and 1100 ppm
- red, if the concentration is above 1100 ppm

## Files in this repo

- `co2-ampel.py` is the main application.
- `co2-ampel.service` is a unit file to run co2-ampel.py a systemd service on Ubuntu.
- the folder `housing` contains the box design files for the lasercutter (svg) and the scattering plate for the 3D-Printer (`ampel_scattering_plate.stl`).


## Usage
One has to change the UIDs of the Bricklet in the code of `co2-ampel.py`. There are several ways to build the project:

- One can run the code on a normal workstation by using a [Master-Brick](https://www.tinkerforge.com/en/doc/Hardware/Bricks/Master_Brick.html) (connected over USB with the workstation). The C02 and the RGB-LED bricklet can be connected directly to the master brick
- With a [RED-Brick](https://www.tinkerforge.com/en/doc/Hardware/Bricks/RED_Brick.html), which in principal a small computer, stacked with a [Master-Brick](https://www.tinkerforge.com/en/doc/Hardware/Bricks/Master_Brick.html), to which the C02 and the RGB-LED bricklet are directly connected. The code can then be uploaded to the RED-Brick. This solution provides a standalone solution without.
- A Raspberry-Pi Zero together with a [HAT Zero Brick](https://www.tinkerforge.com/en/doc/Hardware/Bricks/HAT_Zero_Brick.html) allows a direct connection of the  C02 and the RGB-LED bricklet without the need for a RED-Brick or a Master-Brick. It makes sense to run the code as a systemd services:

    - Copy the file co2-ampel.py into the directory `\usr\local\bin` and make it executable (`chmod a+x`).
    - Copy the content of `co2-ampel.service` into the file `/etc/systemd/system/co2-ampel.service`
    - Enabble the service `sudo systemctl daemon reload` followed by `sudo systemctl enable co2-ampel`
    - Start the service with `sudo systemctl start co2-ampel`


## Authors and acknowledgment
This project has been realized by the members of the Freifach Makerspace HS 21

## License
This project is licensed under the MIT license.


