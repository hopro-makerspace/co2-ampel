HOST = "localhost"
PORT = 4223
UIDRGB = "Ktw"
UIDCO2 = "Me1"

# Import modules
import time
from tinkerforge.ip_connection import IPConnection
from tinkerforge.bricklet_co2_v2 import BrickletCO2V2
from tinkerforge.bricklet_rgb_led_v2 import BrickletRGBLEDV2


def greenLED():
    """sets the LED to green"""
    rgb.set_rgb_value(0, 255, 0)

def orangeLED():
    """sets the LED to orange"""
    rgb.set_rgb_value(255, 255, 0)

def redLED():
    """sets the LED to red"""
    rgb.set_rgb_value(255, 0, 0)


def co2change(co2Meas, tempMeas, humMeas):
    """sets the LED colors depending on the CO2 levels
    < 900 pm -> green
    900 - 1100 ppm -> orange
    > 1100 ppm -> red
    """
    if co2Meas <= 900:
        greenLED()
    elif co2Meas <=  1100:
        orangeLED()
    else:
        redLED()





if __name__ == "__main__":

    ipcon = IPConnection() # Create IP connection
    co2 = BrickletCO2V2(UIDCO2, ipcon) # Create device object
    rgb = BrickletRGBLEDV2(UIDRGB, ipcon)

    ipcon.connect(HOST, PORT) # Connect to brickd
    # Don't use device before ipcon is connected

    co2.register_callback(co2.CALLBACK_ALL_VALUES, co2change)
    co2.set_all_values_callback_configuration(3000, True)

    input("Press enter to exit\n") # Use raw_input() in Python 2
    ipcon.disconnect()
